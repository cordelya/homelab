# HomeLab

This is a stripped-down set of documentation covering my Home Lab without sharing the more sensitive details that are present in my local documentation.

Everything you see listed here has been set up and configured by me. I have no formal training on system administration, dev-ops, or hardware. I have a minimal amount of semi-formal training on development. A vast majority of my development skills are self-taught.

This document covers the long-running stuff. I set up and try out applications on a regular basis, but don't keep plenty of them for lack of use case or because they didn't run well on my systems, or various other reasons.

## On-Premises

### Hardware

  * Servers
    * 2 x Raspberry Pi Model B (2011)[^1]
    * 2 x Raspberry Pi 3
    * 2 x Raspberry Pi 4 (1x 2GB & 1x 8GB)
    * 3 x Raspberry Pi Zero W
    * 3 x Libre AML-S905X-CC codename "Le Potato" (plus one additional in reserve / not in use)
    * 2 x rescued desktop towers
  * Embedded Systems
    * Weather Station - transmits sensor readings to OpenHAB via MQTT broker
      * Feather S3 by Unexpected Maker
      * Adafruit STEMMA Sensors: LPS-22, SCD-30, PMSA003I
      * Custom-designed 3D-printed enclosure
    * E-Paper Name Badge - Choose from six name display options which only need power (USB-C) while changing the display
      * Feather S3 by Unexpected Maker
      * Adafruit 2.9" E-Ink FeatherWing

[^1]: Following the Raspberry Pi Foundation's decision to hire a cop I no longer purchase hardware from them. Pre-existing boards will be run until they break because we don't waste resources here.

### Software

* List of Applications/Services
  * Plex
  * Calibre
  * WebTrees
  * PiHole
  * Tailscale
  * Prometheus
  * Grafana
  * Mobiliaire Inventory Manager (multiple instances for tracking multiple inventories)
  * RetroPie
  * Local Modded Minecraft Server(s)
  * Local Terraria Server
  * Mosquitto MQTT Broker
  * Tiny Tiny RSS
  * WakAPI
  * Vigil
  * Heimdall
  * Shaarli
  * Monica CRM
  * Firefly 3
  * PHP IPAM
  * Huginn
  * OpenHAB
  * Ansible
  * FediBlockHole
  * JupyterLab
* List of Automated/Scheduled scripts
  * A Python script (runs Monday mornings) which examines a Google Calendar .ics file, extracts everything happening in the next 7 days, formats it pretty, and posts the formatted event list to an email list and a Discord server.
  * A Python script (runs twice a week) which examines a Google Calendar .ics file, and sends an alert if any events are found matching specific criteria (looking for events specific keywords in the event name).
* List of Not-Applications
  * Multiple of the above listed machines have had local SSL certificates issued to them by my own personal certificate authority. The matching certificate is available for download on the local network. 

## Off-Premises

  * WordPress MultiSite
  * NextCloud
  * Hugo + GitLab Pages
  * Hugo + AWS Amplify
  * Mastodon @ masto.host

---

## Footnotes